﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SpellCompletion
{
    public partial class Form1 : Form
    {
        string dictionaryPath = @"C:\Program Files\TortoiseGit\Languages\en_US.dic";
        const int minCompletionLength = 3;
        List<string> dictionaryEntries = new List<string>();
        string curFile = null;
        bool changed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines(dictionaryPath);
            char[] chars = { '/', '\t' };
            foreach (var line in lines)
            {
                if (line.Length == 0) continue;
                int idx = line.IndexOfAny(chars);
                if (idx >= 0)
                    dictionaryEntries.Add(line.Substring(0, idx));
                else
                    dictionaryEntries.Add(line);
            }
            dictionaryEntries.Add("its");
            dictionaryEntries.Sort();

            text1.Focus();
        }

        static int LastIndexOfNotLetters(string s, int pos)
        {
            if (s.Length == 0) return -1;
            for (int i = pos; i >= 0; i--)
            {
                if (!char.IsLetter(s[i]))
                    return i;
            }
            return -1;
        }

        private void text1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z')
            {
                string all = text1.Text;
                int idx = LastIndexOfNotLetters(all, text1.SelectionStart - 1);
                int len = text1.SelectionStart - idx - 1;
                if (len >= minCompletionLength - 1)
                {
                    string find = all.Substring(idx + 1, len) + e.KeyChar;
                    list1.Items.Clear();
                    var entries = dictionaryEntries.FindAll(a => a.StartsWith(find, StringComparison.OrdinalIgnoreCase)).ToArray();
                    list1.Items.AddRange(entries);
                    if (entries.Length > 0)
                        list1.SelectedIndex = 0;
                    var pt = text1.GetPositionFromCharIndex(idx + 1);
                    list1.Location = new Point(pt.X, pt.Y + text1.Font.Height + 2);
                    list1.Visible = true;
                }
                else
                    list1.Visible = false;
            }
            else if (e.KeyChar == '\r' || e.KeyChar == ' ')
            {
                if (list1.Visible)
                {
                    string target = list1.Text;
                    if (target.Length > 0)
                    {
                        string all = text1.Text;
                        int idx = LastIndexOfNotLetters(all, text1.SelectionStart - 1);
                        int len = text1.SelectionStart - idx - 1;
                        text1.SelectionStart = idx + 1;
                        text1.SelectionLength = len;
                        text1.SelectedText = target + " ";
                        list1.Visible = false;
                        text1.Focus();

                        e.Handled = true;
                    }
                }
            }
            else
                list1.Visible = false;
        }

        private void text1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                if (list1.Visible)
                {
                    if (list1.SelectedIndex + 1 < list1.Items.Count)
                        list1.SelectedIndex++;
                    e.Handled = true;
                }
            }
            if (e.KeyCode == Keys.Up)
            {
                if (list1.Visible)
                {
                    if (list1.SelectedIndex - 1 > 0)
                        list1.SelectedIndex--;
                    e.Handled = true;
                }
            }
        }

        private void list1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                text1_KeyPress(text1, new KeyPressEventArgs('\r'));
            }
        }

        private bool CheckFileSaved()
        {
            if (!changed)
                return true;
            var result = MessageBox.Show("Save file?", this.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
                return false;
            if (result == DialogResult.Yes && !SaveFile())
                return false;
            return true;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckFileSaved()) return;

            text1.Text = "";
            curFile = null;
            this.Text = this.ProductName;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckFileSaved()) return;
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;

            text1.Text = File.ReadAllText(openFileDialog1.FileName);
            curFile = openFileDialog1.FileName;
            this.Text = curFile + " - " + this.ProductName;
        }

        private bool SaveFile()
        {
            if (File.Exists(curFile))
            {
                try
                {
                    File.WriteAllText(curFile, text1.Text);
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), this.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return SaveFileAs();
        }

        private bool SaveFileAs()
        {
            if (saveFileDialog1.ShowDialog() != DialogResult.OK) return false;
            try
            {
                File.WriteAllText(saveFileDialog1.FileName, text1.Text);
                curFile = saveFileDialog1.FileName;
                this.Text = curFile + " - " + this.ProductName;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), this.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileAs();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!CheckFileSaved()) e.Cancel = true;
        }
    }
}
